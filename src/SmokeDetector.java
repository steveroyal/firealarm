
public class SmokeDetector {


    private int totalRooms=10;
    private Room[] rooms = new Room[totalRooms];
    private float threshold;


    public SmokeDetector() {

    }

    public SmokeDetector(float threshold){
        this.threshold=threshold;
    }

    public int getRoomCount(){
        int count=0;
       for(int i=0;i<totalRooms;i++){
           if(rooms[i]!=null){
               count++;
           }
       }
      return count;
    }

    public boolean addRoomToBeMonitored(Room room){

        if(getRoomCount()<totalRooms) {
            int lastIndex = (getRoomCount()==0)?0:getRoomCount();
            rooms[lastIndex] = room;
            return true;
        }
        else
        return false;

    }

    public Room[] getRooms() {
        return rooms;
    }

    public void setRooms(Room[] rooms) {
        this.rooms = rooms;
    }

    public float getThreshold() {
        return threshold;
    }

    public void setThreshold(float threshold) {
        this.threshold = threshold;
    }

    public boolean checkAlarm(float smokeLevel) {
        if (smokeLevel > threshold)
            return true;
        else
            return false;
    }

}
