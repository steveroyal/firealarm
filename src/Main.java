public class Main {

    public static void main (String[]args){

        Room room2,room1;
        SmokeDetector smokeDetector=new SmokeDetector(50);

        room1=new Room();
     room1.setName("Lab C");
     room1.setCapacity(20);
     room2 = new Room("Lab B",10);


     smokeDetector.addRoomToBeMonitored(room1);
     smokeDetector.addRoomToBeMonitored(room2);

     displayAlarmMsg(smokeDetector.checkAlarm(75),smokeDetector);
     displayAlarmMsg(smokeDetector.checkAlarm(33),smokeDetector);



    }

    static void displayAlarmMsg(boolean alarm,SmokeDetector smokeDetector){
            if (alarm){
                System.out.printf("%d rooms are at risk \n ", smokeDetector.getRoomCount());
                for (int i = 0; i < smokeDetector.getRoomCount(); i++) {
                    smokeDetector.getRooms()[i].display();
                    System.out.println();
                }
            }
            else {
                System.out.printf("%d rooms are safe \n ", smokeDetector.getRoomCount());
                for (int i = 0; i < smokeDetector.getRoomCount(); i++) {
                    smokeDetector.getRooms()[i].display();
                    System.out.println();
                }
            }
    }


}

