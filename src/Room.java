public class Room {

    private String name;
    private int capacity;


    public Room() {
    }

    public Room(Room room){
        this.name=room.name;
        this.capacity=room.capacity;
    }

    public Room(String name,int capacity){
        this.name=name;
        this.capacity=capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void display(){
    System.out.printf("Room name : %s \n Room Capacity : %d",this.name,this.capacity);
    }
}
